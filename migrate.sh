#!/bin/bash
## This script is executed in the auto script after creation of instance and before deletion ##
## This script is not finished, only working for non live migrations right now but the code exists to expand onto this ##
## Do Not Edit on Server ##
#homepath="${BASH_SOURCE%/*}"
homepath="$(cd "$(dirname "$0")" && pwd)"
source $homepath/settings.sh
source $homepath/functions.sh
source $rcfile
instance=$2

showhypervisor() {
hypervisor=$(openstack server show $instance | grep OS-EXT-SRV-ATTR:hypervisor_hostname | awk '{ print $4}')
echo $hypervisor
}

duplicatecheck(){
if  openstack server show $instance 2>&1 | grep -q "More than one server exists with the name '$instance'"; then
msg_web="⛔ - $company - Migration Failed with error: More than one server exists with the name '$instance' - Delete instance and try again.."
          echo $msg_web
                webhookfunction "$msg_web"
exit -1
fi
}

instancecheck(){
if  openstack server show $instance 2>&1 | grep -q "No server with a name or ID of '$instance' exists"; then
msg_web="⛔ - $company - Migration Failed with error: No server with a name or ID of '$instance' exists - Create instance and try again.."
          echo $msg_web
          webhookfunction "$msg_web"
exit -1
fi
}
instancecheck
duplicatecheck
if [[ $1 = "migrate" ]]; then
if [[ $live = "yes" ]]; then
if [[ $targethost = "auto" ]]; then
targethost=
fi
showhypervisor
if [[ $blockmigrate == "no" ]]; then
openstack server migrate --live $targethost $instance
elif [[ $blockmigrate == "yes" ]]; then
openstack server migrate --live $targethost $instance
fi
showhypervisor
## live = no is tested and working ##
elif [[ $live == "no" ]]; then
echo "Migrated from"; showhypervisor
openstack server migrate $instance
  until [ "$(openstack server show $instance | grep status | awk '{print $4}' | tr -d '[:space:]')" == "ERROR" ] || [ "$(openstack server show $instance | grep status | awk '{print $4}' | tr -d '[:space:]')" == "VERIFY_RESIZE" ]        
do
instancecheck
duplicatecheck
done
if  openstack server show $instance | grep -q ERROR; then
msg_web="openstack server show $instance - Shows an ERROR, Migration Failed for $company"
echo $msg_web
         webhookfunction "$msg_web"
exit -1
elif openstack server show $instance | grep -q VERIFY_RESIZE; then
echo 'Migrated to'; showhypervisor
echo 'Confirming resize...'
## Some customers support nova others the openstack cli ## 
openstack server resize --confirm $instance
##nova resize-confirm $instance
sleep $3
if  openstack server show $instance | grep -q ACTIVE; then
echo "Migration Successfull"
else
exit -1
echo "Migration Failed"
fi


fi

fi
fi
if [[ $1 = "hypervisor" ]]; then
showhypervisor
fi
############################
