#!/bin/bash
#homepath="${BASH_SOURCE%/*}"
homepath="$(cd "$(dirname "$0")" && pwd)"
date=$(date +%Y-%m-%d-%H:%M:%S)
source $homepath/settings.sh
source $rcfile
openstackrandomid=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1 | tr '[:upper:]' '[:lower:]')
openstackname="openstack-test-$openstackrandomid"
skipdeletevolume=0
skipdeletenetwork=0
skipdeleteinstance=0
volumecreatetime=0
volumedeletetime=0
networkcreatetime=0
networkdeletetime=0
instancecreatetime=0
instancedeletetime=0
skipmigrateinstance=0
skipcreateinstace=0
skipcreatevolume=0
migrationtime=0
skipped_host=0
mismatch_host=0
services_not_up=0
suppressmismatch=0


msg_web="$company - OpenStack Daily Health Check\n $openstackname\n"
msg_web+="$openstackname\n"

checkauthurl () {
  if  openstack server list 2>&1 | grep -q "Missing value auth-url required for auth plugin password"; then
skipdeletevolume=1
skipdeletenetwork=1
skipdeleteinstance=1
skipmigrateinstance=1
skipcreateinstace=1
skipcreatevolume=1
msg_web+="⚠️ - Missing value auth-url required for auth plugin password, skipping all checks"
 fi
}
checkauthurl

webhookfunction () {
if [[ $webhookurl == *"webhook.site"* ]]; then
curl -H "Content-Type: application/json" -X POST -d $"$1" $webhookurl
elif [[ $webhookurl == *"slack.com"* ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"OpenStack Automation\", \"text\": \"$1\", \"icon_emoji\": \":ghost:\"}" $webhookurl
else
echo "Use Slack or Webhooks.site for webhook url"
exit 1
fi
echo 'Webhook Sent'
}

instance () {
  if [[ $1 = "create" ]]
  then
  openstack server create --image $image --flavor $flavor --network $network $openstackname
  elif [[ $1 = "show" ]]
  then
  openstack server show $openstackname --fit-width

  elif [[ $1 = "delete" ]]
  then
  openstack server delete $openstackname
  else
  echo "Error, use instance create or instance show or instance.sh delete"
  exit 1
  fi
}

volume () {
  if [[ $1 = "create" ]]
  then
  openstack volume create --size 1 $openstackname
  elif [[ $1 = "show" ]]
  then
  openstack volume show $openstackname

  elif [[ $1 = "delete" ]]
  then
  openstack volume delete $openstackname
  else
  echo "Error, use volume.sh create or volume.sh show or volume.sh delete"
  exit 1
  fi
}

network () {
  if [[ $1 = "create" ]]
  then
  openstack network create $openstackname
  elif [[ $1 = "show" ]]
  then
  openstack network show $openstackname

  elif [[ $1 = "delete" ]]
  then
  openstack network delete $openstackname
  else
  echo "Error, use network.sh create or network.sh show or network.sh delete"
  exit 1
  fi
}


checkemptyvolume () {
  if  volume show 2>&1 | grep -q "No volume with a name"; then
    skipdeletevolume=0
    skipcreatevolume=0
  else
    msg="⚠️ - Creating Volume Skipped because there is already a volume named $openstackname\n"
    echo -e "$msg"
    msg_web+=$msg
    skipdeletevolume=1
    skipcreatevolume=1
 fi
}
checkduplicatevolume () {
  if volume show 2>&1 | grep -q "More than one"; then

    msg="⛔ - Failed with error: More than one volume exists with the name '$openstackname'\n"
    echo -e "$msg"
    msg_web+=$msg
    skipdeletevolume=1
    skipcreatevolume=1
  else
  skipdeletevolume=0
  skipcreatevolume=0
 fi
}




createvolume () {
checkemptyvolume
checkduplicatevolume
 if [[ $skipcreatevolume = 0 ]]; then
SECONDS=0
             if  volume create | grep -q creating; then
              echo "Creating Volume..."
                  until [ "$(volume show | grep status | awk -F '|' '{print $3}' | tr -d '[:space:]')" != "creating" ]
                  do
                      echo "Creating Volume, please wait..."
                  done
    if  volume show 2>&1 | grep -q "More than one"; then

       msg="⛔ - Creating Volume Failed after $SECONDS Seconds with error: More than one volume exists with the name '$openstackname' - Delete volumes and try again..\n"
       echo -e "$msg"
       msg_web+=$msg
       skipdeletevolume=1

    fi

              else

                        msg="⛔ - Skipping Volume Deletion, problem detected\n"
                        echo -e "$msg"
                        msgweb+=$msg
                        skipdeletevolume=1

                fi

fi

if  volume show | grep -q available; then
            msg="✅ - Volume Created Successfully in $SECONDS Seconds\n"
            echo -e "$msg"
            msg_web+=$msg
fi
volumecreatetime=$SECONDS
}


deletevolume () {
  if  volume show 2>&1 | grep -q "More than one"; then

    msg="⛔ - Deleting Volume Failed after $SECONDS Seconds with error: More than one volume exists with the name '$openstackname' - Delete volumes and try again..\n"
    echo -e "$msg"
    msg_web+=$msg
    skipdeletevolume=1

 fi
  SECONDS=0
if [[ $skipdeletevolume = 0 ]]; then
volume delete
until volume show 2>&1 | grep -q "No volume with a name or ID of '$openstackname' exists."; do
echo "Deleting Volume..."
done
msg="✅ - Volume Deleted Successfully in $SECONDS Seconds\n"
echo -e "$msg"
msg_web+=$msg
else
msg="⚠️️ - Skipping Volume Deletion\n"
echo -e "$msg"
msgweb+=$msg
fi
volumedeletetime=$SECONDS
}









createnetwork() {
SECONDS=0
  if  network create | grep -q ACTIVE; then
       msg="✅ - Created Network in $SECONDS Seconds\n"
       echo -e "$msg"
       msg_web+=$msg
       networkcreatetime=$SECONDS


       if  network show 2>&1 | grep -q "More than one"; then
         msg="⛔ - Creating Network Failed with error: More than one network exists with the name '$openstackname' - Delete networks and try again..\n"
         echo -e "$msg"
         msg_web+=$msg
         skipdeletenetwork=1
       fi

else
msg="⛔ - Creating Network Failed after $SECONDS Seconds\n"
networkcreatetime=$SECONDS
echo -e "$msg"
msg_web+=$msg
skipdeletenetwork=1
fi
}


deletenetwork () {
  if  network show 2>&1 | grep -q "More than one"; then
    msg="⛔ - Deleting Network Failed with error: More than one network exists with the name '$openstackname' - Delete networks and try again..\n"
    echo -e "$msg"
    msg_web+=$msg
    skipdeletenetwork=1
  fi
if [[ $skipdeletenetwork = 0 ]]; then
SECONDS=0
network delete
until network show 2>&1 | grep -q "No Network found for $openstackname"; do
echo "Deleting Network..."
done
networkdeletetime=$SECONDS
msg="✅ - Network Deleted Successfully in $networkdeletetime Seconds\n"
echo -e "$msg"
msg_web+=$msg

else
  msg="⚠️️ - Skipping network deletion\n"
  echo -e "$msg"
  msg_web+=$msg
fi


}

createinstance() {
SECONDS=0
if  instance show 2>&1 | grep -q "More than one"; then
  msg="⛔ - Creating Instance Failed with error: More than one server exists with the name '$openstackname' - Delete instance and try again..\n"
  echo -e "$msg"
  msg_web+=$msg
  skipcreateinstace=1
  skipmigrateinstance=1
  skipdeleteinstance=1
fi
if [[ $skipcreateinstace = 0 ]]; then
  if  instance create | grep -q BUILD; then
       echo "Building Instance..."
       until [ "$(instance show | grep status | awk -F '|' '{print $3}' | tr -d '[:space:]')" == "ACTIVE" ] || [ "$(instance show | grep status | awk -F '|' '{print $3}' | tr -d '[:space:]')" == "ERROR" ]
       do
       echo "Building Instance, please wait..."
     done
   fi
     instancecreatetime=$SECONDS
       if  instance show | grep -q ACTIVE; then
            msg="✅ - Instance Build Successfull after $instancecreatetime seconds\n"
            echo -e "$msg"
            msg_web+=$msg
      else
        msg="⛔ - Instance Build Failed after $instancecreatetime seconds\n"
        echo -e "$msg"
        msg_web+=$msg
        skipdeleteinstance=1
        skipmigrateinstance=1
      fi
    fi
}

migrateinstance () {
if [[ $skipmigrateinstance = 0 ]]; then
SECONDS=0
sleeptime=10
  migrationoutput=$($homepath/migrate.sh migrate $openstackname $sleeptime)
  echo -e "$migrationoutput"
  msg_web+=$migrationoutput
  msg_web+="\n"

  if [[ $migrationoutput == *"Migration Successfull"* ]]; then
    migrationtime=$SECONDS
    ((migrationtime=migrationtime-$sleeptime))
    msg="✅ - Migrated Successfully after $migrationtime seconds\n"
    echo -e "$msg"
    msg_web+=$msg
 else
   skipdeleteinstance=1
 fi
else
  msg="⚠️️ - Skipping Migration for investigation...\n"
  echo -e "$msg"
  msg_web+=$msg
fi
}

deleteinstance () {
if [[ $skipdeleteinstance = 0 ]]; then
  if  instance show 2>&1 | grep -q "More than one"; then
    msg="⛔ - Deleting Instance Failed with error: More than one server exists with the name '$openstackname' - Delete instance and try again..\n"
    echo -e "$msg"
    msg_web+=$msg
    skipdeleteinstance=1
 else
SECONDS=0
  instance delete
  until instance show 2>&1 | grep -q "No server with a name or ID of '$openstackname' exists."; do
  echo "Deleting Instance..."
  done
instancedeletetime=$SECONDS
msg="✅ - Instance Deleted Successfully in $instancedeletetime seconds\n"
echo -e "$msg"
msg_web+=$msg
fi
else
msg="⚠️️ - Skipping deletion of instance for investigation...\n"
echo -e "$msg"
msg_web+=$msg
fi
}


#fullbuild{
# createvolume
# deletevolume
# createnetwork
# createinstance ($network)
# migrateinstance
# deleteinstance
# deletenetwork
#}

cleanup () {

for i in $(openstack network list | grep openstack-test-.* | awk '{ print $4 }'); do 
id=$(openstack network list | grep $i | awk '{ print $2 }')
if [ ${#i} = "23" ]; then
openstack network show $id >  $homepath/logs/cleanup-network-$id.txt;
openstack network delete $id;
fi
done

for i in $(openstack server list | grep openstack-test-.* | awk '{ print $4 }'); do 
id=$(openstack server list | grep $i | awk '{ print $2 }')
if [ ${#i} = "23" ]; then
openstack server show $id >  $homepath/logs/cleanup-server-$id.txt;
openstack server delete $id;
fi
done

for i in $(openstack volume list | grep openstack-test-.* | awk '{ print $4 }'); do
id=$(openstack volume list | grep $i | awk '{ print $2 }')
if [ ${#i} = "23" ]; then
openstack volume show $id >  $homepath/logs/cleanup-volume-$id.txt;
openstack volume delete $id;
fi
done

}

if [[ $1 = "--cleanup" ]]
then
cleanup
fi




timereport () {
msg_web+="Volume creation time: $volumecreatetime Seconds\n"
msg_web+="Volume deletion time: $volumedeletetime Seconds\n"
msg_web+="Network creation time: $networkcreatetime Seconds\n"
msg_web+="Network deletion time: $networkdeletetime Seconds\n"
msg_web+="Instance creation time: $instancecreatetime Seconds\n"
msg_web+="Instance deletion time: $instancedeletetime Seconds\n"
msg_web+="Migration time: $migrationtime Seconds\n"
SECONDSTOTAL=$(($volumecreatetime + $volumedeletetime + $networkcreatetime + $networkdeletetime + $instancecreatetime + $instancedeletetime + $migrationtime))
msg_web+="Total time for script to execute = $SECONDSTOTAL Seconds\n"
}

computeservicelist () {

if [[ $(openstack compute service list | grep -v "up" | tail -n +4 | head -n -1) ]]; then
computeservicelist=$(openstack compute service list --long -c Binary -c Host -c Zone -c Status -c 'State' -c 'Disabled Reason' | grep -v up;)
    msg="⛔ - Some compute services are not up! \n"
    msg+="\`\`\`$computeservicelist\`\`\`"
    echo -e "$msg"
    msg_web+=$msg
else
    msg="✅ - Compute service check complete, everything is up\n"
    echo -e "$msg"
    msg_web+=$msg
fi

}



computeservicelist2 () {
if [[ $(openstack compute service list -f csv | grep -E '(down|disabled)' > $homepath/facsl.tmp; cat $homepath/facsl.tmp) ]]; then

for servicenotup in $(cat $homepath/facsl.tmp); do
servicenotup_binary=$(echo $servicenotup | awk -F ',"' '{ print $2 }' | sed 's/"//g')
servicenotup_hostname=$(echo $servicenotup | awk -F ',"' '{ print $3 }' | sed 's/"//g')
servicenotup_status=$(echo $servicenotup | awk -F ',"' '{ print $5}' | sed 's/"//g')
servicenotup_state=$(echo $servicenotup | awk -F ',"' '{ print $6}' | sed 's/"//g')
ki=$(grep ",$servicenotup_hostname," $homepath/known_issues.config | grep $servicenotup_binary | grep -v '#')
if [[ $(echo $ki) ]]; then
ki_date=$(echo $ki | awk -F ',' '{ print $1}')
ki_removaldate=$(echo $ki | awk -F ',' '{ print $2}')
ki_host=$(echo $ki | awk -F ',' '{ print $3}')
ki_service=$(echo $ki | awk -F ',' '{ print $4}')
ki_status=$(echo $ki | awk -F ',' '{ print $5}')
ki_state=$(echo $ki | awk -F ',' '{ print $6}')
ki_reason=$(echo $ki | awk -F ',' '{ print $7}')
ki_suppressmismatch=$(echo $ki | awk -F ',' '{ print $8}')
if [ $ki_host = $servicenotup_hostname ] && [ $ki_status = $servicenotup_status ] && [ $ki_state = $servicenotup_state ]; then
skipped_host=1
else 
if [[ "$ki_suppressmismatch" -eq 1 ]]; then
suppressmismatch=1
else 
mismatch_host=1
fi
fi
else 
### Host not found in known_issues.config
services_not_up=1
fi
done

if [[ "$mismatch_host" -eq 1 ]]; then
 msg="⛔ - Mismatch in compute services and known_issues.config - see mismatch.log for more information\n"
printf "### Start ### \n Actual State: $servicenotup_hostname, $servicenotup_status, $servicenotup_state \n Known Issue config: $ki_host,$ki_status,$ki_state \n### End ###\n" >> $homepath/logs/mismatch_$date.txt
### ^^^makes invalid payload in webhookfunction, investigate
    echo -e "$msg"
    msg_web+=$msg
fi

if [[ "$suppressmismatch" -eq 1 ]]; then
 msg="⚠️ - Some mismatch alerts have been suppressed known_issues.config - see mismatch.log for more information\n"
    echo -e "$msg"
    msg_web+=$msg
fi

if [[ "$services_not_up" -eq 1 ]]; then
computeservicelist=$(openstack compute service list --long -c Binary -c Host -c Zone -c Status -c 'State' -c 'Disabled Reason' | grep -E '(down|disabled)')
    msg="⛔ - Some compute services are either down or disabled! \n"
    msg+="\n\`\`\`$computeservicelist\`\`\`\n"
    echo -e "$msg"
    msg_web+=$msg
fi

if [[ "$skipped_host" -eq 1 ]]; then
msg="✅ - Some compute services were skipped due to an EXACT match on the known_issues.config\n"
echo -e "$msg"
msg_web+=$msg
fi


else
    msg="✅ - Compute service check complete, everything is up\n"
    echo -e "$msg"
    msg_web+=$msg
fi

}

volumeservicelist () {

if [[ $(openstack volume service list | grep -v "up" | tail -n +4 | head -n -1) ]]; then
volumeservicelist=$(openstack volume service list -c Binary -c Host -c Zone -c Status -c 'State' | grep -v up;)
    msg="⛔ - Some volume services are not up! \n"
    ### Workaround to the format problem - Slack ``` text here ``` wouldn't work for volume service list even though it was the same code as compute service list
    echo "$volumeservicelist" > volumeservicelist.txt
    while read vsl; do
  msg+="\`$vsl\`\n"
done < volumeservicelist.txt
rm -f volumeservicelist.txt
    echo -e "$msg"
    msg_web+=$msg
else
    msg="✅ - Volume service check complete, everything is up\n"
    echo -e "$msg"
    msg_web+=$msg
fi

}

networkagentlist () {

if [[ $(openstack network agent list | grep -v ":-)" | tail -n +4 | head -n -1) ]]; then
networkagentlist=$(openstack network agent list -c 'Host' -c 'Agent Type' -c 'State' -c Binary -c 'Alive' | grep -v ':-)')
msg="⛔ - Some network agents are not up!\n"
    ### Workaround to the format problem - Slack ``` text here ``` wouldn't work for network agent list even though it was the same code as compute service list
    echo "$networkagentlist" > networkagentlist.txt
    while read nal; do
  msg+="\`$nal\`\n"
done < networkagentlist.txt
rm -f networkagentlist.txt
    echo -e "$msg"
    msg_web+=$msg 
else
    msg="✅ - Network agent check complete, everything is up\n"
    echo -e "$msg"
    msg_web+=$msg
fi

}


<<com
plans for the known config to auto cleanup expired entries & also to check for duplicate entries

knownissueexpirecleanup () {

for kic in $(cat known_issues.config | grep -v '#'); do

ki_removaldate=$(echo $kic | awk -F ',' '{ print $2}')



done


}

knownissueduplicatecheck () {
}
com





