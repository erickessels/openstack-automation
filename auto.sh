#!/bin/bash
#homepath="${BASH_SOURCE%/*}"
homepath="$(cd "$(dirname "$0")" && pwd)"
source $homepath/functions.sh
date=$(date +%Y-%m-%d-%H:%M:%S)

#########################################
createvolume
deletevolume
createnetwork
deletenetwork
createinstance
migrateinstance
deleteinstance
#computeservicelist
computeservicelist2
networkagentlist
volumeservicelist
timereport
#########################################

msg_web+="openstack-automation V1.5.2.5\n"
webhookfunction "$msg_web"
echo -e "$msg_web" > $homepath/logs/auto-$date.txt
exit 0
