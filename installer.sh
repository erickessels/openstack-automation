#!/bin/bash
envpath=$(env | grep ^PATH=)
homepath="$(cd "$(dirname "$0")" && pwd)"
chmod +x $homepath/*.sh
mkdir -p $homepath/logs
sed -i "s~PATH=~$envpath~g" $homepath/settings.sh
echo 'Customer Company Name:'
read customername
sed -i "s/company=/company='$customername'/g" $homepath/settings.sh

echo 'Full Path to Openstack RC file:'
read opecrcfile
sed -i "s~rcfile=~rcfile='$opecrcfile'~g" $homepath/settings.sh

echo 'Slack Channel:'
read slackchannel
sed -i "s/slackchannel=/slackchannel='$slackchannel'/g" $homepath/settings.sh

echo 'Slack Webhook URL:'
read webhookurl
sed -i "s~webhookurl=~webhookurl='$webhookurl'~g" $homepath/settings.sh


echo 'Openstack Image ID:'
read osimageid
sed -i "s/image=/image='$osimageid'/g" $homepath/settings.sh

echo 'Openstack Flavor ID:'
read osflavorid
sed -i "s/flavor=/flavor='$osflavorid'/g" $homepath/settings.sh

echo 'Openstack Network ID:'
read osnetworkid
sed -i "s/network=/network='$osnetworkid'/g" $homepath/settings.sh

read -p "Done! press any key to see your config. If you made a mistake, edit the settings.sh file"

cat $homepath/settings.sh

crontab -l > cronfile
echo "00 6 * * * $homepath/auto.sh >> $homepath/logs/cron-\`date +\%Y\-\%m\-\%d\-\%H\:\%M\:\%S\`.txt 2>&1" >> cronfile
echo "00 13 * * * $homepath/auto.sh >> $homepath/logs/cron-\`date +\%Y\-\%m\-\%d\-\%H\:\%M\:\%S\`.txt 2>&1" >> cronfile
echo "0 0 * * * $homepath/functions.sh --cleanup" >> cronfile
crontab cronfile
rm cronfile
rm -- "$0"